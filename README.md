# Movie Chatbot

This project contains a jupyter notebook (google colab) of a chatbot that answers questions related to movies.

# Description

This project was conducted in the scope of the Advanced AI subject at UZH. The goal was to develop a chatbot that is based on a knowledge graph. Based on the questions he dynamically forms a query of the knowledge graph and then returns an answer in written language. This chatbot is not based on deeplearning methods. It uses heuristics which are also considered machine learning. An examplary chat conducted with the chatbot can be found in the pdf file, which also entails a more technical description.
